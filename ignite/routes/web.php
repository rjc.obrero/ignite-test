<?php
use Illuminate\Support\Facades\Route;

Route::get('/', 'IgniteController@search');
Route::get('export', 'IgniteController@export');
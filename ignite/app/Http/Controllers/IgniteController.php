<?php
namespace App\Http\Controllers;

use Response;
use Http;
use App\Http\Controllers\Controller;
use App\User;

class IgniteController extends Controller
{
	public function get_data()
	{
		$endpoint = "https://ignite-careers.com/test/endpoint.php";
		$id = 10;
		$response = Http::withOptions([
			'verify' => false,
		])->withToken('qwertyuiop')
		->post($endpoint, [
			'jobId' => $id,
		]);
		
		$content = $response->getBody()->read(2048);
		$content = json_decode($content,1);
		$content = $content['data'];
		
		return $content;
	}
	 
    public function search()
    {
		$data['records'] = $this->get_data();
		return view('index',$data);
    }
	
	public function export()
	{
		$fileName = 'job-php.csv';
		$records = $this->get_data();
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('ID', 'First Name', 'Last Name', 'Mobile', 'Email');

        $callback = function() use($records, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($records as $record) {
                fputcsv($file, $record);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
	}
}
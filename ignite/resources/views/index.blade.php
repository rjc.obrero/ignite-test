<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
	
	<h2>PHP Output</h2>
	<table border="1">
		<thead>
		<th>ID</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Mobile</th>
		<th>Email</th>
	</thead>
	@foreach($records as $record)
	<tr>
		<td>{{ $record['uid'] }}</td>
		<td>{{ $record['first_name'] }}</td>
		<td>{{ $record['last_name'] }}</td>
		<td>{{ $record['mobile'] }}</td>
		<td>{{ $record['email'] }}</td>
	</tr>
	@endforeach
	</table>
	<br>
	<a href="http://localhost/ignite/ignite/public/export" target="_blank"><button>Export as CSV</button></a>
	<br>
	<br>
	<br>
	<hr>
	<h2>JS Output</h2>
    <div id="table-container">
	</div>
	<br>
	<button id="export">Export as CSV (JS)</button>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script>
	$.ajax
	({
		type: "POST",
		url: "https://ignite-careers.com/test/endpoint.php",
		headers: {
			"Authorization": "Bearer qwertyuiop",
		},
		data:'{"jobId":10}',
		success: function (response){
			var jobs = response.data;
			
			var table = '<table border="1"  class="dataTable"><tr>' + 
			'<th>ID</th>' +
			'<th>First Name</th>' +
			'<th>Last Name</th>' +
			'<th>Mobile</th>' +
			'<th>Email</th>' +
			'</tr>';
			
			$.each(jobs, function (idx, obj) {                                   
				table += ('<tr>');
				table += ('<td>' + obj.uid + '</td>');
				table += ('<td>' + obj.first_name + '</td>');
				table += ('<td>' + obj.last_name + '</td>');
				table += ('<td>' + obj.mobile + '</td>');
				table += ('<td>' + obj.email + '</td>');
				table += ('</tr>');
			});
			
			table += '</table>';
			$("#table-container").html(table);
		}
	});
	
	$('#export').click(function() {
	  var titles = [];
	  var data = [];

	  /*
	   * Get the table headers, this will be CSV headers
	   * The count of headers will be CSV string separator
	   */
	  $('.dataTable th').each(function() {
		titles.push($(this).text());
	  });

	  /*
	   * Get the actual data, this will contain all the data, in 1 array
	   */
	  $('.dataTable td').each(function() {
		data.push($(this).text());
	  });
	  
	  /*
	   * Convert our data to CSV string
	   */
	  var CSVString = prepCSVRow(titles, titles.length, '');
	  CSVString = prepCSVRow(data, titles.length, CSVString);

	  /*
	   * Make CSV downloadable
	   */
	  var downloadLink = document.createElement("a");
	  var blob = new Blob(["\ufeff", CSVString]);
	  var url = URL.createObjectURL(blob);
	  downloadLink.href = url;
	  downloadLink.download = "jobs-js.csv";

	  /*
	   * Actually download CSV
	   */
	  document.body.appendChild(downloadLink);
	  downloadLink.click();
	  document.body.removeChild(downloadLink);
	});

	   /*
	* Convert data array to CSV string
	* @param arr {Array} - the actual data
	* @param columnCount {Number} - the amount to split the data into columns
	* @param initial {String} - initial string to append to CSV string
	* return {String} - ready CSV string
	*/
	function prepCSVRow(arr, columnCount, initial) {
	  var row = ''; // this will hold data
	  var delimeter = ','; // data slice separator, in excel it's `;`, in usual CSv it's `,`
	  var newLine = '\r\n'; // newline separator for CSV row

	  /*
	   * Convert [1,2,3,4] into [[1,2], [3,4]] while count is 2
	   * @param _arr {Array} - the actual array to split
	   * @param _count {Number} - the amount to split
	   * return {Array} - splitted array
	   */
	  function splitArray(_arr, _count) {
		var splitted = [];
		var result = [];
		_arr.forEach(function(item, idx) {
		  if ((idx + 1) % _count === 0) {
			splitted.push(item);
			result.push(splitted);
			splitted = [];
		  } else {
			splitted.push(item);
		  }
		});
		return result;
	  }
	  var plainArr = splitArray(arr, columnCount);
	  // don't know how to explain this
	  // you just have to like follow the code
	  // and you understand, it's pretty simple
	  // it converts `['a', 'b', 'c']` to `a,b,c` string
	  plainArr.forEach(function(arrItem) {
		arrItem.forEach(function(item, idx) {
		  row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
		});
		row += newLine;
	  });
	  return initial + row;
	}
	</script>
    </body>
</html>
